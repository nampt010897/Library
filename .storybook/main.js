const path = require("path");
const fs = require("fs");
const { merge } = require("webpack-merge");
const packageMaps = {
  
}

function getPackageDir(filepath) {
  let currDir = path.dirname(require.resolve(filepath))
  while(true) {
    if (fs.existsSync(path.join(currDir, "package.json"))) {
      return currDir;
    }
    const { dir, root } = path.parse(currDir);
    if (dir === root) {
      throw new Error(
        `Could not find package.json in the parent directories staring from ${filepath}.`
      );
    }
    currDir = dir;
  }
}

function scopeSetter(defaultStories) {
  const indexOfScope = process.argv.indexOf('scope');
  const args = indexOfScope > -1 ? process.argv.slice(indexOfScope +1) : []

  const stories = args.reduce((acc, scope) => ([
    ...acc,
    ...[
      `../packages/stories/src/${packageMaps[scope] || scope}/*.stories.mdx`,
      `../packages/stories/src/${packageMaps[scope] || scope}/*.stories.@(js|jsx|ts|tsx)`
    ],
  ]), [])

  return stories.length > 0 ? stories : defaultStories;
}

module.exports = {
  "stories": ["../packages/stories/**/*.stories.@(js|jsx|ts|tsx)"],
  "addons": [
    {
      name: "@storybook/addon-essentials",
      options: {
        backgrounds: false
      }
    },
    "@storybook/addon-links",
    "@storybook/addon-options",
    "@storybook/addon-knobs",
    "@storybook/addon-controls",
    "storybook-readme",
    "storybook/addon-viewport",
    "@react-theming/storybook-addon",
    "@storybook/addon-a11y",
    "storybook-addon-performance/register"
  ],
  "framework": "@storybook/react",
  "webpackFinal": async (config) => {
    return merge(config, {
      resolve: {
        alias: {
          "@emotion/core": getPackageDir("@emotion/react"),
          "@emotion/styled": getPackageDir("@emotion/styled"),
          "@emotion/theming": getPackageDir("@emotion/react"),
        }
      }
    })
  },
}