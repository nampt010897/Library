import { registerDsm } from '@invisionapp/dsm-storybook/register';
import { configure, addDecorator, addParameters } from '@storybook/react';
import { withA11y } from '@storybook/addon-a11y';
import {withKnobs} from '@storybook/addon-knobs';
import '!style-loader!css-loader!./styles.css';
import { withPerformance } from 'storybook-addon-performance';
import { addReadme } from 'storybook-readme';
import { initDsm} from '@invisionapp/dsm-storybook';
import { bgTheme } from '../packages/themes/src';
import { ThemeProvider } from '@emotion/react';
import { withThemes } from '@react-theming/storybook-addon'
import { INITIAL_VIEWPORTS } from '@storybook/addon-viewport';

let selectedTheme = null;

registerDsm(process.env.STORYBOOK_DSM);

const {
  iphone5,
  galaxys5,
  iphone6,
  iphonex,
  iphonexsmax,
  ipad,
  ipad10p,
  ipad12p,
  ...OTHER_VIEWPORTS
} = INITIAL_VIEWPORTS;

addParameters({
  options: {
    panelPosition: 'right',
    selectedPanel: 'storybookjs/knobs/panel',
    showRoot: true,
    storySort: {
      order: ['Welcome']
    }
  },
  viewport: {
    viewports: {
      iphone5,
      galaxys5,
      iphone6,
      iphonex,
      iphonexsmax,
      ipad,
      ipad10p,
      ipad12p,
    },
  },
});

addDecorator(addReadme);
addDecorator(withPerformance);
addDecorator(withA11y);
addDecorator(
  withKnobs({
    escapeHTML: false,
  })
)

addDecorator(withThemes(null, [bgTheme], {
  providerFn: ({ theme, children }) => {
    if (window.parent && window.parent.document && window.parent.document.getElementById) {
      const themingButton = window.parent.document.getElementById('tabbutton-theming');
      if (themingButton && themingButton.classList.contains('tabbutton-active')) {
        selectedTheme = theme;
      }
    }

    return <ThemeProvider theme={selectedTheme || theme}>{children}</ThemeProvider>
  }
}))

const loadFn = () => {
  const req = require.context('../packages/stories/src', true, /\.stories\.(tsx|js)$/);
  
  var x = req.keys();
  
  var y = [];
  for(var i = 0;i < x.length;i++){
    y[i] = req(x[i]);
  }

  return y;
}

initDsm({
  addDecorator,
  addParameters,
  callback: () => configure(loadFn, module)
});