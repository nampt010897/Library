module.exports = {
    'presets': [
      '@babel/preset-env',
      '@babel/preset-react',
      '@babel/preset-typescript'
    ],
    'ignore': [
      'node_modules'
    ],
    'plugins': [
      ['@emotion', {
        'autoLabel': 'never'
      }],
      'dynamic-import-node',
      '@babel/plugin-transform-runtime',
      '@babel/plugin-proposal-class-properties',
      '@babel/plugin-proposal-private-methods',
    ],
    "env": {
      "test": {
        'presets': [
          '@babel/preset-env',
          '@babel/preset-react',
          '@babel/preset-typescript'
        ],
        "plugins": [
          'transform-export-extensions'
        ]
      },
      "es6": {
        'presets': [
          ['@babel/preset-env', { modules: false }]
        ],
      }
    },
    "sourceType": "unambiguous"
  }
