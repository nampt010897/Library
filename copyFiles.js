const fs = require('fs-extra');

const dirs = fs.readdirSync('./packages').filter(dirName => dirName.charAt(0) !== '.' && dirName !== 'stories');

dirs.forEach(dir => {
    const source = `./packages/${dir}/src/package.json`;
    const destination = `./packages/${dir}/dist/package.json`;

    if (fs.existsSync(source)) {
        fs.copyFile(source, destination, (err) => {
            if (err) throw err;
            console.log(`${dir} package.json copied to dist folder`);
        })
    } else {
        console.warn(`WARNING: package.json not found in directory ${dir}`);
    }
})