module.exports = {
    globals: {
        NODE_ENV: 'test',
        'ts-jest': {
            extends: './babel.config.js',
            diagnostics: {
                wanrnOnly: true
            }
        }
    },
    coveragePathIgnorePatterns: [
        '/.storybook/',
        '/docs/',
        '/fonts/',
        '/mixins/',
        '/SVGIcons/',
        '/stories/',
        '/node_modules/',
        '/dist/',
        '/js.snap',
        '.config.js',
        'package.json',
        'package-lock.json',
    ],
    moduleNameMapper: {
        '\\.(css|scss)$': 'identity-obj-proxy',
        '@library/(.+)$': '<rootDir>packages/$1/src',
    },
    coverageReporters: ['json', 'lcov', 'text', 'clover', 'html'],
    moduleFileExtensions: ['ts','tsx','js','jsx','json'],
    testPathIgnorePatterns: ['<rootDir>/node_modules'],
    berbose: false,
    snapshotSerializers: ['jest-emotion/serialier'],
    testMatch: ['**/__test__/*.+(ts|tsx|js)', '**/*.test.+(ts|tsx|js)'],
    transform: {
        '^.+\\.js$' : 'babel-jest',
        '^.+\\.(ts|tsx)$': 'ts-jest',
    },
};