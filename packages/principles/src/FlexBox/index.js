import styled from '@emotion/styled';
import React from 'react';

import breakpoints from '../constants/breakpoints';
import size from '../constants/size';
import { isBlockElement, spaceSetting } from '../utils';

const flexItemStyles = (itemsFlex, horizontalSpacing) =>
  itemsFlex.reduce((acc, flexValue, index) => {
    const flexValues = flexValue
      .toString()
      .replace(',', '')
      .split(' ');
    const flexBasis = flexValues.length > 2 && flexValues[2];

    if (flexBasis) {
      flexValues[2] =
        flexBasis.indexOf('%') > -1
          ? `calc(${flexBasis} - ${size[horizontalSpacing]}px)`
          : flexBasis;
    }

    return `${acc}
    > *:nth-of-type(${index + 1}) {
      flex: ${flexValues.join(' ')};
      ${
  flexBasis
    ? `flex-basis: calc(${flexBasis} - ${size[horizontalSpacing]}px)`
    : ''
}
    }
  `;
  }, '');

const getMediaQuery = (breakpoint) => {
  const breakpointKeys = Object.keys(breakpoints);
  const nextIndex = breakpointKeys.indexOf(breakpoint) + 1;
  const nextBreakpoint = breakpoints[breakpointKeys[nextIndex]];

  if (nextBreakpoint) {
    return `@media (min-width: ${
      breakpoints[breakpoint]
    }px) and (max-width: ${nextBreakpoint - 1}px)`;
  }

  return `@media (min-width: ${breakpoints[breakpoint]}px)`;
};

const flexBoxStyles = ({
  flowColumn,
  flexWrap,
  alignItems,
  justifyContent,
  horizontalSpacing,
  ...props
}) => {
  const mediaQueries = Object.keys(props).reduce((acc, breakpoint) => {
    if (breakpoints[breakpoint] && props[breakpoint]) {
      return `${acc} ${getMediaQuery(breakpoint)} {
        ${flexItemStyles(props[breakpoint], horizontalSpacing)}
      }
      `;
    }

    return acc;
  }, '');

  return `
    display: flex;
    flex-direction: ${flowColumn ? 'column' : 'row'};
    ${flexWrap ? `flex-wrap: ${flexWrap};` : ''}
    ${alignItems ? `align-items: ${alignItems};` : ''}
    ${justifyContent ? `justify-content: ${justifyContent};` : ''}
    ${
  props.default
    ? `${flexItemStyles(props.default, horizontalSpacing)}${mediaQueries}`
    : mediaQueries
}
  `;
};

const ContentContainer = styled.div`
  ${props => spaceSetting({ ...props, flex: true })}
  ${props => flexBoxStyles(props)}
`;

const OverflowContainer = styled.div`
  ${({ containerWidth }) => (containerWidth ? `width: ${containerWidth};` : '')}
  ${({ hideOverflow = false }) =>
    hideOverflow ? 'overflow: hidden;' : 'overflow: unset;'}
`;

const getChild = child =>
  isBlockElement(child) || !child ? child : <div>{child}</div>;

const FlexBox = ({ dataLeanAuto, ...rest }) => (
  <OverflowContainer
    containerWidth={rest.containerWidth}
    hideOverflow={rest.hideOverflow}>
    <ContentContainer {...rest} {...(dataLeanAuto ? { 'data-lean-auto': dataLeanAuto } : {})}>
      {React.Children.toArray(rest.children).map(child => getChild(child))}
    </ContentContainer>
  </OverflowContainer>
);

FlexBox.displayName = 'FlexBox';

FlexBox.defaultProps = {
  horizontalSpacing: 'sm',
  verticalSpacing: 'sm',
};

export default FlexBox;
