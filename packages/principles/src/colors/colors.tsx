import { bgTheme } from '@library/themes';

const {
  primary,
  green,
  orange,
  magenta,
  purple,
  gold,
  red,
  neutral,
  action,
  blue
} = bgTheme.colors;

export {
    primary,
    green,
    orange,
    magenta,
    purple,
    gold,
    red,
    neutral,
    action,
    blue
  }