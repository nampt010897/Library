import { bgTheme } from "@library/themes";

const {
  sm,
  md,
  lg,
  xl,
  max,
} = bgTheme.breakpoints;

export default {
  sm,
  md,
  lg,
  xl,
  max,
};