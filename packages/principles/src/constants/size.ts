import { bgTheme } from '@library/themes';

const {
  none,
  tiny,
  micro,
  xs,
  xs12,
  sm,
  md,
  lg,
  xl,
  mdx2,
  xxl,
  xxxl,
  yuge,
  contentMaxWidth,
  globalNavHeight,
} = bgTheme.size;

export default {
  none,
  tiny,
  micro,
  xs,
  xs12,
  sm,
  md,
  lg,
  xl,
  mdx2,
  xxl,
  xxxl,
  yuge,
  contentMaxWidth,
  globalNavHeight,
};