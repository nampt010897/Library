export default {
  base: '10%',
  sm: '20%',
  md: '45%',
  lg: '50%'
};