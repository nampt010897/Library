import { bgTheme } from '@library/themes';

const {
  family,
  weight,
  lineHeightDefaults,
  fontColorDefaults,
  defaults,
  types,
  get,
} = bgTheme.typography;

export {
  family,
  weight,
  lineHeightDefaults,
  fontColorDefaults,
  defaults,
  types,
  get,
};
