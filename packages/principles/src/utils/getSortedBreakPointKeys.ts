import breakpoints from "../constants/breakpoints";

export default objectWithBreakpointsAsKeys => {
  return Object.keys(breakpoints).filter(key =>
    objectWithBreakpointsAsKeys.hasOwnProperty(key)
  );
}