
import { Children } from 'react';
import { inlineElements, size} from '../constants';

export const isBlockElement = element => 
  element &&
  inlineElements.indexOf(element.type) === -1 &&
  typeof element !== 'string';

export const spaceSetting = ({
  verticalSpacing = 'sm',
  horizontalSpacing = 'sm',
  children = [],
  flex = false,
}) => {
  const verticalMargin = (size[verticalSpacing] || 0) / (flex ? 2 : 1);

  return `
    margin: 0px -${(size[horizontalSpacing] || 0) / 2}px;

    ${
      isBlockElement(children[0]) || flex 
      ? `margin-top: -${verticalMargin}px;`
      : ''
    }

    > * {
      margin: ${verticalMargin}px ${(size[horizontalSpacing] || 0) / 2}px;
    }
  `;
}