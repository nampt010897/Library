import React from 'react';
import { storiesOf } from '@storybook/react';
import styled from '@emotion/styled';
import { typography } from '@library/principles';
import { DownloadFonts } from '@library/utils';
import { Heading } from '@library/ui-elements'
import FlexBox from '../../../principles/src/FlexBox';

const Wrapper = styled.div`
  ${typography.types['bodyXL']};
`;

storiesOf('Welcome/to Storybook', module).add('to Storybook', () => (
  <Wrapper datatype={'description'} style={{ width: '80%', margin: 'auto'}}>
    {/* <DownloadFonts/> */}
    <FlexBox flowColumn>
      <Heading enableTitleCase headingName="PageHeading">
        Welcome to Library's Storybook
      </Heading>
    </FlexBox>
  </Wrapper>
))