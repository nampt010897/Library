export const primary = {
    black: '#000000',
    charcoal: '#505050',
    offWhite: '#F2F2F2',
    white: '#ffffff',
    marineBlue: '#1f3e74',
    airForceBlue: '#2b5195',
    riverBlue: '#5586d4',
    tag: 'Primary',
  };
  
  export const green = {
    base: '#379302',
    light: '#b1ed8e',
    pastel: '#e9ffdd',
    dark: '#296f01',
    tag: 'Green',
  };
  
  export const orange = {
    base: '#f48036',
    light: '#ffcc85',
    pastel: '#fff4cf',
    dark: '#b94c09',
    tag: 'Orange',
  };
  
  export const magenta = {
    base: '#ba1d4c',
    tag: 'Magenta',
  };
  
  export const purple = {
    base: '#681f74',
    tag: 'Purple',
  };
  
  export const gold = {
    base: '#f3bc2a',
    light: '#F8D472',
    dark: '#BE8B09',
    tag: 'Gold',
  };
  
  export const red = {
    base: '#f43a36',
    light: '#ffaba8',
    dark: '#ba0e0a',
    pastel: '#feeeee',
    tag: 'Red',
  };
  
  export const neutral = {
    black: primary.black,
    charcoal: '#505050',
    darkGray: '#939393',
    trueGray: '#c0c0c0',
    stoneGray: '#dcdcdc',
    doveGray: '#ebebeb',
    offWhite: primary.offWhite,
    white: primary.white,
    tag: 'Neutral',
  };
  
  export const action = {
    gold: gold.base,
    brightBlue: '#0799f8',
    lightBlue: '#e6f5fe',
    darkBrightBlue: '#006BC2',
    tag: 'Action',
  };
  
  export const blue = {
    navy: '#162d5b',
    marineBlue: primary.marineBlue,
    airForceBlue: primary.airForceBlue,
    mutedBlue: '#406cb4',
    riverBlue: primary.riverBlue,
    babyBlue: action.lightBlue, //baby blue
    lightBrightBlue: '#6ac2fb',
    darkBrightBlue: action.darkBrightBlue,
    tag: 'Blue',
  };
  
  export const externalBrand = {
    applePrimary1: '#000000',
    applePrimary2: '#ffffff',
    facebookPrimary1: '#1877f2',
    facebookPrimary2: '#000000',
    facebookPrimary3: '#ffffff',
    googlePrimary1: '#000000',
    googlePrimary2: '#ffffff',
  };