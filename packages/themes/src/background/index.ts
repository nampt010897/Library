import colors from "./colors";
import size from './shared/size';
import breakpoints from './shared/breakpoints';
import typography from './typography';
import { css } from "@emotion/react";

const { 
  gold,
  blue,
  neutral,
  action,
} = colors;

export default {
  name: 'IELTS',
  global: {
    fontFamily: 'Open Sans, sans-serif, Tahoma, Arial',
  },
  size,
  breakpoints,
  typography,
  colors,
  badge: {
    typography: typography.types.XXS,
  },
  button: {
    primary: {
      bg: gold.base,
      color: neutral.black,
      large: 48,
      regular: 48,
      small: 32,
      fontWeight: 800,
    },
    secondary: {
      bg: blue.darkBrightBlue,
      color: neutral.white,
      large: 48,
      regular: 48,
      small: 32,
    },
    tertiary: {
      bg: neutral.white,
      border: `2px solid ${action.darkBrightBlue}`,
      color: action.darkBrightBlue,
      large: 48,
      regular: 48,
      small: 28,
    },
    circleSizes: {
      large: 32,
      regular: 28,
      small: 24,
    },
    defaultFontWeight: 600,
    fontFamily: typography.family.montserrat,
    fontSize: '18px',
    borderRadius: '4px',
    fontSizeNormal: '18px',
    borderRadiusLarge: '4px',
    paddingLarge: '32px',
    paddingNormal: '12px',
    verticalPaddingSmall: '8px',
    sizeText: 24,
    tertiaryColor: action.darkBrightBlue,
  },
  breadcrumbs: {
    padding: '0 2px',
    typography: typography.types.Breadcrumb,
  },
  buttonIcon: {
    background: '',
    iconColor: `${colors.action.brightBlue}`,
    primaryHover: `box-shadow: 0px 10px 20px 0px rgb(0 0 0 / 15%);`,
    primaryFocus: `box-shadow: 0 0 8px rgb(0, 107, 194, 1);`
  },
  carousel: {
    numericIndicatorText: typography.types.XSSemi,
    strokeColor: colors.action.darkBrightBlue,
    chevronColor: colors.action.darkBrightBlue,
  },
  tab: {
    typography: css`
    ${typography.types.XSSemi}
      &.active {
        ${typography.types.XSExtra}
      }
    `,
    deselectedColor: `${colors.neutral.black}`,
    deselectedInverseColor: `${colors.primary.white}`,
    selectedFontColor: `${colors.neutral.black}`,
    selectedInverseColor: `${colors.primary.white}`,
  },
  tabs: {
    selectedFontColor: `${colors.action.brightBlue}`,
    selectedInverseColor: `${colors.action.brightBlue}`,
  },
  icon: {
    iconColor: `${colors.action.darkBrightBlue}`,
    InverseColor: `${colors.action.darkBrightBlue}`,
  },
  overlayBackground: {
    background: css`background: rgba(0, 0, 0, 0.9)`,
    zIndex: 10030,
  },
  disclaimer: {
    color: neutral.charcoal,
    inverseColor: neutral.white,
    defaultTypographyPreset: 'bodySmall',
  }
};