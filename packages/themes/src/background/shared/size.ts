export default {
  none: 0,
  tiny: 2,
  micro: 4,
  xs: 8,
  xs12: 12,
  sm: 16,
  md: 24,
  lg: 32,
  xl: 40,
  mdx2: 48,
  xxl: 56,
  xxxl: 64,
  yuge: 80,
  contentMaxWidth: 1280,
  globalNavHeight: 80,
};