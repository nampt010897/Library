import { SerializedStyles } from '@emotion/react';

import { buildGet } from '../../utils/typography/get';
import { buildTypes } from '../../utils/typography/types';
import colors from '../colors';

const montserratFallback = process.env.MONTSERRAT_FALLBACK;

export interface Typography {
  '3XL': string | SerializedStyles;
  '2XL': string | SerializedStyles;
  XL: string | SerializedStyles;
  large: string | SerializedStyles;
  MExtra: string | SerializedStyles;
  MSemi: string | SerializedStyles;
  SExtra: string | SerializedStyles;
  SSemi: string | SerializedStyles;
  XSExtra: string | SerializedStyles;
  XSSemi: string | SerializedStyles;
  XSMedium: string | SerializedStyles;
  XXS: string | SerializedStyles;
  bodyXL: string | SerializedStyles;
  bodyL: string | SerializedStyles;
  body: string | SerializedStyles;
  bodyWide: string | SerializedStyles;
  bodyTight: string | SerializedStyles;
  body12: string | SerializedStyles;
  bodySmall: string | SerializedStyles;
  SpecialXSExtra: string | SerializedStyles;
  SpecialXSSemi: string | SerializedStyles;
  SpecialXSMedium: string | SerializedStyles;
  Breadcrumb: string | SerializedStyles;
}

export const family = {
  opensans: '"Open Sans" sans-serif, Tahoma, Arial',
  montserrat: `"Montserrat",${montserratFallback ? `${montserratFallback},` : ''} sans-serif`,
};

export const weight = {
  extraBold: '800',
  bold: '700',
  semiBold: '600',
  medium: '500',
  light: '400',
  regular: '400',
};

export const lineHeightDefaults: Typography = {
  '3XL': 'line-height: 72px;',
  '2XL': 'line-height: 64px;',
  XL: 'line-height: 48x;',
  large: 'line-height: 40px;',
  MExtra: 'line-height: 28x;',
  MSemi: 'line-height: 28px;',
  SExtra: 'line-height: 24px;',
  SSemi: 'line-height: 24px;',
  XSExtra: 'line-height: 16px;',
  XSSemi: 'line-height: 16px;',
  XSMedium: 'line-height: 16px;',
  XXS: 'line-height: 12px;',
  bodyXL: 'line-height: 26px;',
  bodyL: 'line-height: 26px;',
  body: 'line-height: 24px;',
  bodyWide: 'line-height: 36px;',
  bodyTight: 'line-height: 18px;',
  body12: 'line-height: 18px;',
  bodySmall: 'line-height: 12px;',
  SpecialXSExtra: 'line-height: 16px;',
  SpecialXSSemi: 'line-height: 16px;',
  SpecialXSMedium: 'line-height: 16px;',
  Breadcrumb: 'line-height: 21px;',
};

export const fontColorDefaults: Typography = {
  '3XL': `color: ${colors.primary.black};`,
  '2XL': `color: ${colors.primary.black};`,
  XL: `color: ${colors.primary.black};`,
  large: `color: ${colors.primary.black};`,
  MExtra: `color: ${colors.primary.black};`,
  MSemi: `color: ${colors.primary.black};`,
  SExtra: `color: ${colors.primary.black};`,
  SSemi: `color: ${colors.primary.black};`,
  XSExtra: `color: ${colors.primary.black};`,
  XSSemi: `color: ${colors.primary.black};`,
  XSMedium: `color: ${colors.primary.black};`,
  XXS: `color: ${colors.primary.black};`,
  bodyXL: `color: ${colors.primary.charcoal};`,
  bodyL: `color: ${colors.primary.charcoal};`,
  body: `color: ${colors.primary.charcoal};`,
  bodyWide: `color: ${colors.primary.charcoal};`,
  bodyTight: `color: ${colors.primary.charcoal};`,
  body12: `color: ${colors.primary.charcoal};`,
  bodySmall: `color: ${colors.primary.charcoal};`,
  SpecialXSExtra: `color: ${colors.primary.black};`,
  SpecialXSSemi: `color: ${colors.primary.black};`,
  SpecialXSMedium: `color: ${colors.primary.black};`,
  Breadcrumb: `color: ${colors.action.darkBrightBlue};`,
};

export const defaults: Typography = {
  '3XL': `
    font-family: ${family.montserrat};
    font-size: 72px;
    font-weight: ${weight.extraBold};
    letter-spacing: 0;
    text-transform: capitalize;
  `,
  '2XL': `
    font-family: ${family.montserrat};
    font-size: 60px;
    font-weight: ${weight.extraBold};
    letter-spacing: 0;
    text-transform: capitalize;
  `,
  XL: `
    font-family: ${family.montserrat};
    font-size: 46px;
    font-weight: ${weight.extraBold};
    letter-spacing: 0;
  `,
  large: `
    font-family: ${family.montserrat};
    font-size: 36px;
    font-weight: ${weight.extraBold};
    letter-spacing: 0;
    text-transform: capitalize;
  `,
  MExtra: `
    font-family: ${family.montserrat};
    font-size: 24px;
    font-weight: ${weight.extraBold};
    letter-spacing: 0;
    text-transform: capitalize;
  `,
  MSemi: `
    font-family: ${family.montserrat};
    font-size: 24px;
    font-weight: ${weight.semiBold};
    letter-spacing: 0;
    text-transform: capitalize;
  `,
  SExtra: `
    font-family: ${family.montserrat};
    font-size: 18px;
    font-weight: ${weight.extraBold};
    letter-spacing: 0;
    text-transform: capitalize;
  `,
  SSemi: `
    font-family: ${family.montserrat};
    font-size: 18px;
    font-weight: ${weight.semiBold};
    letter-spacing: 0;
    text-transform: capitalize;
  `,
  XSExtra: `
    font-family: ${family.montserrat};
    font-size: 14px;
    font-weight: ${weight.extraBold};
    letter-spacing: 0.5px;
  `,
  XSSemi: `
    font-family: ${family.montserrat};
    font-size: 14px;
    font-weight: ${weight.semiBold};
    letter-spacing: 0.5px;
  `,
  XSMedium: `
    font-family: ${family.montserrat};
    font-size: 14px;
    font-weight: ${weight.medium};
    letter-spacing: 0.5px;
  `,
  XXS: `
    font-family: ${family.montserrat};
    font-size: 10px;
    font-weight: ${weight.semiBold};
    letter-spacing: 0;
  `,
  bodyXL: `
    font-family: ${family.opensans};
    font-size: 18px;
    font-weight: ${weight.semiBold};
    letter-spacing: 0px;
  `,
  bodyL: `
    font-family: ${family.opensans};
    font-size: 16px;
    font-weight: ${weight.regular};
    letter-spacing: 0px;
  `,
  body: `
    font-family: ${family.opensans};
    font-size: 14px;
    font-weight: ${weight.regular};
    letter-spacing: 0px;
  `,
  bodyWide: `
    font-family: ${family.opensans};
    font-size: 14px;
    font-weight: ${weight.regular};
    letter-spacing: 0px;
  `,
  bodyTight: `
    font-family: ${family.opensans};
    font-size: 14px;
    font-weight: ${weight.regular};
    letter-spacing: 0px;
  `,
  body12: `
    font-family: ${family.opensans};
    font-size: 14px;
    font-weight: ${weight.regular};
    letter-spacing: 0px;
  `,
  bodySmall: `
    font-family: ${family.opensans};
    font-size: 10px;
    font-weight: ${weight.regular};
    letter-spacing: 0.7px;
  `,
  SpecialXSExtra: `
    font-family: ${family.montserrat};
    font-size: 12px;
    font-weight: ${weight.extraBold};
    letter-spacing: 0.5px;
  `,
  SpecialXSSemi: `
    font-family: ${family.montserrat};
    font-size: 12px;
    font-weight: ${weight.semiBold};
    letter-spacing: 0.5px;
  `,
  SpecialXSMedium: `
    font-family: ${family.montserrat};
    font-size: 12px;
    font-weight: ${weight.medium};
    letter-spacing: 0.5px;
  `,
  Breadcrumb: `
    font-family: ${family.opensans};
    font-size: 12px;
    font-weight: ${weight.regular};
    letter-spacing: 0.5px;
  `,
};

export const types: Typography = buildTypes({
  defaults,
  fontColorDefaults,
  lineHeightDefaults,
});

export const get = buildGet({
  defaults,
  fontColorDefaults,
  lineHeightDefaults,
});
