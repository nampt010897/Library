import { css, SerializedStyles } from '@emotion/react';

export const buildGet = ({
  defaults = {},
  fontColorDefaults = {},
  lineHeightDefaults = {},
}: {
  defaults?: object;
  fontColorDefaults?: object;
  lineHeightDefaults?: object;
}) => {
  const get: (
    type: string,
    withColor?: boolean,
    withLineHeight?: boolean
  ) => SerializedStyles = (type, withColor = false, withLineHeight = false) => {
    if (withColor && withLineHeight) {
      console.warn(`Use typography.types['${type}'] instead of get method`);
    }

    return css`
      ${defaults[type]}
      ${withColor ? fontColorDefaults[type] : ''}
      ${withLineHeight ? lineHeightDefaults[type] : ''}
    `;
  };

  return get;
};
