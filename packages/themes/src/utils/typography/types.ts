import { css } from '@emotion/react';

export const buildTypes = ({
  defaults,
  fontColorDefaults,
  lineHeightDefaults,
}) => Object.keys(defaults).reduce(
  (acc, type) => ({
    ...acc,
    [type]: css`
    ${defaults[type]}
    ${fontColorDefaults[type]}
    ${lineHeightDefaults[type]}
    `,
  }),
  defaults
);
