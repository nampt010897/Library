import { css } from '@emotion/react';
import styled from '@emotion/styled';
import { breakpoints, colors, typography } from '@library/principles';
import { getBakeTitle } from '@library/utils';
import React, { FunctionComponent } from 'react';

import { HeadingProps } from './types';

const renderStyle = (
  viewport: number,
  typoSmallViewport: string,
  typoLargeViewport: string,
  textTransformation: boolean,
  isInverse: boolean,
  lineClamp: number
): any => css`
    ${lineClamp ? `
      display: -webkit-box;
      -webkit-line-clamp: ${lineClamp};
      -webkit-box-orient: vertical;
      overflow: hidden;
    ` : ''}
    ${typography.types[typoSmallViewport]};
    margin: 0;
    ${isInverse ? `color: ${colors.primary.white}` : ''}
    ${textTransformation ? 'text-transform: none;' : ''}
    @media (min-width: ${viewport}px) {
      ${typography.types[typoLargeViewport]};
      ${isInverse ? `color: ${colors.primary.white};` : ''}
      ${textTransformation ? 'text-transform: none;' : ''}
    }
`;

const headingStyles = (textTransformation, isInverse, lineClamp): any => ({
  SuperHeadingStyle: renderStyle(
    breakpoints.xl + 1,
    'large',
    '2XL',
    textTransformation,
    isInverse,
    lineClamp
  ),
  SlHeadingStyle: renderStyle(
    breakpoints.md,
    'XL',
    '2XL',
    textTransformation,
    isInverse,
    lineClamp
  ),
  PageHeadingStyle: renderStyle(
    breakpoints.md,
    'large',
    'XL',
    textTransformation,
    isInverse,
    lineClamp
  ),
  SectionHeadingStyle: renderStyle(
    breakpoints.md,
    'MExtra',
    'large',
    textTransformation,
    isInverse,
    lineClamp
  ),
  SubsectionHeadingExtraStyle: renderStyle(
    breakpoints.md,
    'SExtra',
    'MExtra',
    textTransformation,
    isInverse,
    lineClamp
  ),
  SubsectionHeadingSemiStyle: renderStyle(
    breakpoints.md,
    'SSemi',
    'MSemi',
    textTransformation,
    isInverse,
    lineClamp
  ),
  ContentHeadingExtraStyle: renderStyle(
    breakpoints.lg,
    'XSExtra',
    'SExtra',
    textTransformation,
    isInverse,
    lineClamp
  ),
  ContentHeadingSemiStyle: renderStyle(
    breakpoints.lg,
    'XSSemi',
    'SSemi',
    textTransformation,
    isInverse,
    lineClamp
  ),
});

const StyledHeading1 = styled.h1<HeadingProps>`
  ${({ textTransformation, isInverse, headingName, lineClamp }): string =>
    headingStyles(textTransformation, isInverse, lineClamp)[`${headingName}Style`]}
`;

const StyledHeading2 = styled.h2<HeadingProps>`
  ${({ textTransformation, isInverse, headingName, lineClamp }): string =>
    headingStyles(textTransformation, isInverse, lineClamp)[`${headingName}Style`]}
`;

const StyledHeading3 = styled.h3<HeadingProps>`
  ${({ textTransformation, isInverse, headingName, lineClamp }): string =>
    headingStyles(textTransformation, isInverse, lineClamp)[`${headingName}Style`]}
`;

const StyledHeading4 = styled.h4<HeadingProps>`
  ${({ textTransformation, isInverse, headingName, lineClamp }): string =>
    headingStyles(textTransformation, isInverse, lineClamp)[`${headingName}Style`]}
`;

const Heading: FunctionComponent<HeadingProps> = ({
  children,
  headingTag,
  headingName,
  enableTitleCase,
  ...rest
}) => {
  children = enableTitleCase && children ? getBakeTitle(children) : children;

  switch (headingTag) {
    case 'h2':
      return (
        <StyledHeading2 headingName={headingName} {...rest}>
          {children}
        </StyledHeading2>
      );
    case 'h3':
      return (
        <StyledHeading3 headingName={headingName} {...rest}>
          {children}
        </StyledHeading3>
      );
    case 'h4':
      return (
        <StyledHeading4 headingName={headingName} {...rest}>
          {children}
        </StyledHeading4>
      );
    case 'h1':
    default:
      return (
        <StyledHeading1 headingName={headingName} {...rest}>
          {children}
        </StyledHeading1>
      );
  }
};

Heading.displayName = 'Heading';

Heading.defaultProps = {
  headingTag: 'h1',
  headingName: 'SuperHeading',
  enableTitleCase: true,
  textTransformation: true,
  lineClamp: 0,
};

export default Heading;
