export interface HeadingProps {
  headingTag?: 'h1' | 'h2' | 'h3' | 'h4';
  headingName?:
    | 'SuperHeading'
    | 'SlpHeading'
    | 'PageHeading'
    | 'SectionHeading'
    | 'SubSectionHeadingExtra'
    | 'SubSectionHeadingSemi'
    | 'ContentHeadingExtra'
    | 'ContentHeadingSemi';
  enableTitleCase?: boolean | undefined;
  textTransformation?: boolean | undefined;
  isInverse?: boolean | undefined;
  lineClamp?: number | undefined;
};