import styled from '@emotion/styled';
import { colors, typography } from '@library/principles';

const { primary } = colors;

const StyledParagraph = styled('p')`
  ${props => typography.types[props.type]}

  color: ${props => (props.inverse ? primary.white : primary.charcoal)};
  text-align: ${props => (props.textAlign ? props.textAlign : 'left')};
  ${props => (props.defaultMargin ? '' : 'margin:0;')}

  ${props => props.lineClamp ? `
    display: -webkit-box;
    -webkit-line-clamp: ${props.lineClamp};
    -webkit-box-orient: vertical;
    overflow: hidden;
  ` : ''}

  ${props => (props.wordBreak ? `word-break: ${props.wordBreak};` : '')}
`;

const Paragraph = props => <StyledParagraph {...props} />;

Paragraph.displayName = 'Paragraph';

Paragraph.defaultProps = {
  inverse: false,
  type: 'body',
  textAlign: 'left',
  defaultMargin: false,
  wordBreak: '',
};

export default Paragraph;