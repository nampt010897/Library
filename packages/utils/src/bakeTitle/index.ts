const NON_CAPITALIZE_WORDS = [
  'a',
  'an',
  'the',
  'and',
  'but',
  'if',
  'for',
  'or',
  'nor',
  'at',
  'by',
  'from',
  'in',
  'into',
  'of',
  'on',
  'over',
  'to',
  'with',
];

// Capitalize
const getTitleWord = text =>
  text
    .split('-')
    .map((txt, index, array) =>
      index !== 0 &&
      (NON_CAPITALIZE_WORDS.indexOf(txt.toLowerCase()) > -1 || 
        (array.length === 2 && array[0].length === 1))
        ? txt.toLowerCase()
        : txt.charAt(0).toUpperCase() + txt.substring(1).toLowerCase()
    )
    .join('-');

export const getBakeTitle = text =>
  text && text
    .split(' ')
    .map((txt, index) => 
      txt === txt.toUpperCase()
        ? txt
        : NON_CAPITALIZE_WORDS.indexOf(txt.toLowerCase()) > -1 && index !== 0
        ? txt.toLowerCase()
        :getTitleWord(txt)
    )
    .join(' ');