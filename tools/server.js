import storybook from '@storybook/react/dist/server/middleware';
import express from 'express';
import open from 'open';

const port = 6066;
const app = express();
const configDir = './.storybook';

app.use(storybook(configDir));

app.listen(port, (err) => {
    if (err) {
        console.log(err);
    } else {
        open(`http://localhost:${port}`);
    }
})