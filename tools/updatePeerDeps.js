const fs = require('fs');
const glob = require("glob");

const path = './packages';

glob(path + '/**/**/packages.json', {}, function (err, files) {
  if (err) {
      console.log(err);
  } else {
      changePackages(files);
  }
});

function change(file) {
  fs.readFile(file, (err, data) => {
    if (err) {
      console.log('failed to read ', file);
    }

    const json = JSON.parse(data);
    if (json.peerDependencies && json.devDependences) {
      json.peerDependencies = json.devDependences;
    } else if (json.peerDependencies && !json.devDependences) {
      json.peerDependencies = {...json.peerDependencies, ...json.dependences};
    }
    fs.writeFile(file, JSON.stringify(json, null ,2), (err) => {
      if (err) {
          console.log('failed to update peerDependencies', file);
      }
    });
  });
}

function changePackages(files) {
  const filteredFiles = files.filter((file) => file.indexOf('node_modules') === -1);

  for(var i = 0; i < filteredFiles.length; i++) {
    if (filteredFiles[i].indexOf('stories') === -1) {
        change(filteredFiles[i]);
    }
  }
}