let path = require("path");

module.exports = {
    entry: {
        components: "./src/components"
    },
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "dist.js",
        libraryTarget: "commonjs2",
        library: ["Library"]
    },
    externals: {
        react: "react"
    },
    resolve: {
        extensions: ['.js','.jsx','.json','.ts','.tsx'],
        modules: [path.join(__dirname, "node_modules/react"), "node_modules"]
    },
    module: {
        rules: [
            {
                test: /\.js(x)?$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            },
            {
                test: /\.(ts|tsx)$/,
                use: {
                    loader: require.resolve('babel-loader')
                }
            },
            {
                test: [/\.bmp$/, /\.gif$/, /\.jpe$/, /\.png$/, /\.svg$/],
                use: {
                    loader: require.resolve('url-loader'),
                    options: {
                        limit: 10000,
                        name: 'static/media/[name].[hash:8].[ext]'
                    }
                }
            }
        ]
    },
    node: {
        fs: 'empty'
    }
}